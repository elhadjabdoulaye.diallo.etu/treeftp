### Projet n°1: Application Tree FTP

### Objectif : 
L'objectif du projet est de mettre en œuvre un commande shell permettant d'afficher sur la sortie standard d'un terminal  
l'arborescence d'un répertoire distant accessible via le protocole applicatif File Transfer Protocol (FTP).

### Author: DIALLO Elhadj

**7/01/2022**

#### Introduction
La réalisation de ce projet 1 a necessité la mise en place d'un client ftp, qui via une socket se connect au serveur FTP dont l'adresse a été fourni par l'utilisateur sur la ligne de commande, avec le port 21 codé dans la classe du client ftp comme étant une constante.

#### Architecture
Voire UML des classes ici ![alt text](./doc/treeftp.png)
#### Code Samples

```java
	// l'envoie d'une commande sur le serveur
    public void sendCommandToServer(String cmd) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        PrintWriter printer = new PrintWriter(out, true);
        printer.println(cmd+"\r\n");
    }

    //la lecture sur le même canal
    public String readResponse(){
        try {
            InputStream input = this.socket.getInputStream();
            InputStreamReader InputReader = new InputStreamReader(input);
            BufferedReader reader = new BufferedReader(InputReader);
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);;
        }
        return null;
    }
    
    //Pour parcourir un dossier sur le serveur en ces étapes :
    // 1. l'activation du mode passif en envoyant la commande PASV
    // 2. le serveur renvoie une adresse ip et un nouveau port
    // 3. l'ouverture d'un nouveau canal
    // 4. l'envoie de la commande LIST
    // 5. parcours de cette liste puis l'appel recursif sur le dossier lu
    private void dirBrowser(String dir, FtpClient clientSocket) throws IOException {
        clientSocket.sendCommandToServer("PASV");
        //sending the command PASV
        String response = clientSocket.readResponse();
        if(response.startsWith("227 ")) {
            int new_port;
            String new_ip;
            int opening = response.indexOf('(');
            int closing = response.indexOf(')');
            String link = response.substring(opening + 1, closing);
            StringTokenizer tokens = new StringTokenizer(link, ",");
            try {
                new_ip = tokens.nextToken() + '.' + tokens.nextToken() + '.'
                        + tokens.nextToken() + '.' + tokens.nextToken();
                new_port = Integer.parseInt(tokens.nextToken()) * 256 +
                        Integer.parseInt(tokens.nextToken());
            } catch (Exception ex) {
                throw new IOException("Bad data link information");
            }
            Socket dataSocket = new Socket(new_ip, new_port);
            clientSocket.sendCommandToServer("LIST ");
            clientSocket.readResponse();
            String dataLine;

            BufferedReader dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
            while ((dataLine = dataReader.readLine()) != null) {
                if (isDirectory(dataLine)) {
                    displayDirOrFile(dirName(dataLine));
                    String file = dirName(dataLine);
                    clientSocket.sendCommandToServer("CWD " + file);
                    clientSocket.readResponse();
                    dirBrowser(file, clientSocket);
                    clientSocket.sendCommandToServer("CDUP "+file);
                    clientSocket.readResponse();
                }
            }
        }

    }
```

### Problèmes rencontrés:

Tous les dossiers ne sont pas explorés, ce problème ne provoque aucune erreur mais selon la connexion que j'utilise le serveur répond différemment.

Quand je travaille chez moi, les serveurs ftp ne sont pas accessibles via le wifi eduroam donc j'utilise mes données mobiles.

A chaque exécution du programme, j'ai un affichage des dossiers différents donc je n'ai pas compris la cause réelle de cela.
