package treeftp;

import java.io.*;
import java.net.Socket;

/**
 * @author <h3></h3>
 */
public class FtpClient {
    private Socket socket;
    private User user;
    private final int PORT = 21;

    private String host;

    public FtpClient(User user, String host) {
        this.user = user;
        this.host = host;
    }

    public FtpClient(String host) {
        this.host = host;
    }

    /**
     * <p>Connexion to the server with the host and the port</p>
     */
    public void connexionToServer(){
        try{
            this.socket =  new Socket(host, PORT);
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * <p>This function allow to send the command to the server.</p>
     * @param cmd <i>the command name</i>
     * @throws IOException
     */
    public void sendCommandToServer(String cmd) throws IOException {
        OutputStream out = this.socket.getOutputStream();
        PrintWriter printer = new PrintWriter(out, true);
        printer.println(cmd+"\r\n");
    }

    /**
     * <p>This function read a line from the server once
     * the connexion is established.</p>
     * @return String
     */
    public String readResponse(){
        try {
            InputStream input = this.socket.getInputStream();
            InputStreamReader InputReader = new InputStreamReader(input);
            BufferedReader reader = new BufferedReader(InputReader);
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Socket getSocket() {
        return socket;
    }

}
