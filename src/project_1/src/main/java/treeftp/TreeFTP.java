package treeftp;

import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.List;

/**
 * @author <h3>DIALLO Elhadj</h3>
 */
public class TreeFTP {
    private FtpClient client;
    private static int cpt = 0;
    public static final String TEXT_BLUE = "\u001B[34m";
    /**
     * <p>Constructor when username and user password are supplied.</p>
     * @param host FTP server address
     * @param username the username
     * @param user_pwd the user's password
     */
    public TreeFTP(String host, String username, String user_pwd){
        User user = new User(username, user_pwd);
        this.client = new FtpClient(user, host);
    }

    /**
     * <p>Constructor when username and user password are not supplied.</p>
     * @param host FTP server address
     */
    public TreeFTP(String host){
        this.client = new FtpClient(host);

    }

    /**
     * <p>Connexion to the FTP server with username and user password</p>
     * @param username the username
     * @param user_pwd the user's password
     * @throws IOException
     */
    public void connexionToFTPServerWithUserNameAndUserPwd(String username, String user_pwd) throws IOException {
        this.client.connexionToServer();
        String response = this.client.readResponse();
        if(!response.startsWith("220 ")){
            throw new IOException("unknown response received when connecting to the FTP server: "+response);
        }

        //sending command USER
        this.client.sendCommandToServer("USER "+username);
        response = this.client.readResponse();
        if(!response.startsWith("331 ")){
            throw new IOException("unknown response received after sending the username: "+response);
        }

        //sending user password
        this.client.sendCommandToServer("PASS "+user_pwd);
        response = this.client.readResponse();
        if(!response.startsWith("230 ")){
            throw new IOException("unknown response received after sending the user password: "+response);
        }
    }

    /**
     * <p>Connexion to the ftp server with only the host</p>
     * @throws IOException
     */
    public void connexionToFTPServerWithHost() throws IOException {
        this.connexionToFTPServerWithUserNameAndUserPwd("anonymous", "anonymous");
    }
    /**
     * <p>Connexion to the ftp server with only the host</p>
     * @throws IOException
     */
    public void connexionToFTPServerWithUser(String username, String user_pwd) throws IOException {
        this.connexionToFTPServerWithUserNameAndUserPwd(username, user_pwd);
    }
    /**
     * <p>This function allow to display the tree of the servers directories</p>
     * @throws IOException
     */
    public void tree() throws IOException {
        dirBrowser("/", this.client);
    }

    /**
     * <p>this function return the filename or directory from the string that
     * got by reading the content of a list after sending the LIST command</p>
     * @param dirInfo <i>The information of a directory</i>
     * @return String
     */
    private String dirName(String dirInfo){
        List<String> toks = Arrays.asList(dirInfo.split(" "));
        return toks.get(toks.size() - 1);
    }

    /**
     * <p>This function display the name of the directory or the file</p>
     * @param name <i>the directory or file name</i>
     */
    private void displayDirOrFile(String name){
        String space = " ".repeat(cpt);
        if(name.indexOf('.') > 0) {
            System.out.print(" |");
            System.out.println(" '--" + name);
        }
        else {
            System.out.println(space+"|"+name+TEXT_BLUE);
        }

    }

    /**
     * <p>This allow to browse the directory passed as parameter</p>
     * @param dir <i>the directory name</i>
     * @throws IOException <i>An exception is throwing whe there is no socket</i>
     */
    private void dirBrowser(String dir, FtpClient clientSocket) throws IOException {
        clientSocket.sendCommandToServer("PASV");
        //sending the command PASV
        String response = clientSocket.readResponse();
        if(response.startsWith("227 ")) {
            int new_port;
            String new_ip;
            int opening = response.indexOf('(');
            int closing = response.indexOf(')');
            String link = response.substring(opening + 1, closing);
            StringTokenizer tokens = new StringTokenizer(link, ",");
            try {
                new_ip = tokens.nextToken() + '.' + tokens.nextToken() + '.'
                        + tokens.nextToken() + '.' + tokens.nextToken();
                new_port = Integer.parseInt(tokens.nextToken()) * 256 +
                        Integer.parseInt(tokens.nextToken());
            } catch (Exception ex) {
                throw new IOException("Bad data link information");
            }
            Socket dataSocket = new Socket(new_ip, new_port);
            clientSocket.sendCommandToServer("LIST ");
            clientSocket.readResponse();
            String dataLine;

            BufferedReader dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
            while ((dataLine = dataReader.readLine()) != null) {
                displayDirOrFile(dirName(dataLine));
                if (isDirectory(dataLine)) {
                    String file = dirName(dataLine);
                    clientSocket.sendCommandToServer("CWD " + file);
                    clientSocket.readResponse();
                    dirBrowser(file, clientSocket);
                    clientSocket.sendCommandToServer("CDUP "+file);
                    clientSocket.readResponse();
                }
            }
        }

    }

    public Boolean isDirectory(String filename){
        return filename.startsWith("d");
    }
}
