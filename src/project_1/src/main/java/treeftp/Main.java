package treeftp;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class Main
{
    public static void main( String[] args ) {
        TreeFTP treeFTP;
        if(args.length == 1) {
            treeFTP = new TreeFTP(args[0]);
            try {
                treeFTP.connexionToFTPServerWithHost();
                treeFTP.tree();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }else {
            treeFTP = new TreeFTP(args[0], args[1], args[2]);
            try {
                treeFTP.connexionToFTPServerWithUserNameAndUserPwd(args[1], args[2]);
                treeFTP.tree();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
